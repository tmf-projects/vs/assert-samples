﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Text.RegularExpressions;

namespace AssertSamples.Tests
{
    [TestClass]
    public class StringAssertMethods
    {
        [TestMethod]
        public void StringContainsTest()
        {
            // проверка на вхождение в строку подстроки
            StringAssert.Contains("Assert samples", "sam");
        }

        [TestMethod]
        public void StringMatchesTest()
        {
            // Проверка с использованием регулярного выражения
            StringAssert.Matches("123", new Regex(@"\d{3}"));
        }

        [TestMethod]
        public void StringStartWithTest()
        {
            // проверка с того, что строка начинается с определённого слова
            StringAssert.StartsWith("Hello world", "Hello");
        }

        [TestMethod]
        public void StringEndsWithTest()
        {
            // проверка с того, что строка заканчивается на определённом слове
            StringAssert.EndsWith("Hello world", "world");
        }
    }
}
