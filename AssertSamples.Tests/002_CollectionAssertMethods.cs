﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace AssertSamples.Tests
{
    [TestClass]
    public class CollectionAssertMethods
    {
        static List<string> employees;

        [ClassInitialize]
        public static void InitializeCurrentTest(TestContext testContext)
        {
            employees = new List<string>();

            employees.Add("Ivan");
            employees.Add("Sergey");
            employees.Add("Anton");
            employees.Add("Roman");
        }

        [TestMethod]
        public void AllItemsAreNotNullTest()
        {
            // проверка, что все элементы коллекции созданы
            CollectionAssert.AllItemsAreNotNull(employees, "Not null failed");
        }

        [TestMethod]
        public void AllItemsAreUniqueTest()
        {
            // Проверка значений коллекции на уникальность
            CollectionAssert.AllItemsAreUnique(employees, "Uniqueness failed");
        }

        [TestMethod]
        public void AreEqualTest()
        {
            List<string> employeesTest = new List<string>();

            employeesTest.Add("Sergey");
            employeesTest.Add("Ivan");
            employeesTest.Add("Anton");
            employeesTest.Add("Roman");

            // Проверка каждого элемента на равенство, здесь 1-й элемент
            // не совпадает с 1-м элементом из employeesTest
            CollectionAssert.AreEqual(employees, employeesTest);
        }

        [TestMethod]
        public void AreEquivalentTest()
        {
            List<string> employeesTest = new List<string>();

            employeesTest.Add("Sergey");
            employeesTest.Add("Ivan");
            employeesTest.Add("Anton");
            employeesTest.Add("Roman");

            // Проверка коллекций на наличие одинаковых элементов,
            // порядок элементов не имеет значения
            CollectionAssert.AreEquivalent(employees, employeesTest);
        }

        [TestMethod]
        public void EmployeesSubsetTest()
        {
            List<string> employees_Subset = new List<string>();

            employees_Subset.Add(employees[2]);
            // employees_Subset.Add("Alexander"); // если убрать комментарий -
            // тест закончится с ошибкой

            CollectionAssert.IsSubsetOf(employees_Subset, employees, "failed!");
        }
    }
}
