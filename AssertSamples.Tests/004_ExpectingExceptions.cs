﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AssertSamples.Tests
{
    [TestClass]
    public class ExpectingExceptions
    {
        // ExpectedException - тест будет успешным, если в процессе вып-я будет введено null
        [ExpectedException(typeof(ArgumentNullException), "Exception was not throw...")]
        [TestMethod]
        public void MyClass_SayHello_Exception()
        {
            MyClass instance = new MyClass();
            instance.SayHello(null);
        }

        [TestMethod]
        public void MyClass_SayHello_ReturnDmitriy()
        {
            MyClass instance = new MyClass();
            string expected = "Hello Dmitriy";

            string actual = instance.SayHello("Dmitriy");

            Assert.AreEqual(expected, actual);
        }
    }
}
