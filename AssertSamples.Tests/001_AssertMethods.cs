﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace AssertSamples.Tests
{
    [TestClass]
    public class AssertMethods
    {
        [TestMethod]
        public void IsSqrtTest()
        {
            const double input = 4;
            const double expected = 2;

            double actual = MyClass.GetSqrt(input);

            Assert.AreEqual(expected, actual, "Sqrt of {0} should have been {1}!", input, expected);
        }

        [TestMethod]
        public void DeltaTest()
        {
            const double expected = 3.1;
            const double delta = 0.07;

            // 3.1622...
            // 0.062
            double actual = MyClass.GetSqrt(10);

            // проверка значений на равенство с учётом погрешности delta
            Assert.AreEqual(expected, actual, delta, "fail message!");
        }

        [TestMethod]
        public void StringAreEqualTest()
        {
            const string input = "HELLO";
            const string expected = "hello";

            // третий параметр - игнорирование регистра
            Assert.AreEqual(expected, input, true);
        }

        [TestMethod]
        public void StringSameTest()
        {
            string a = "hello";
            string b = string.Intern("hello");

            // проверка равенства ссылок
            Assert.AreSame(a, b);
        }

        [TestMethod]
        public void IntengersSameTest()
        {
            int i = 10;
            int j = 10;

            // проверка равенства ссылок (тут ссылок нет, поэтому тест завершится с ошибкой)
            Assert.AreSame(i, j);
        }
    }
}

